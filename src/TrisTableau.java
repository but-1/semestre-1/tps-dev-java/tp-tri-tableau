import java.util.Arrays;

/**
 * Cette classe effectue des op\u00e9rations plus complexes sur un tableaux d'entiers : recherche dichotomique, tris, etc. La taille d'un tableau est
 * par d\u00e9finition le nombre TOTAL de cases = tab.length. Un tableau d'entiers cr\u00e9\u00e9 poss\u00e8de nbElem \u00e9lements qui ne correspond pas
 * forc\u00e9ment \u00e0 la taille du tableau. Il faut donc toujours consid\u00e9r\u00e9 que nbElem &lt;= tab.length (= taille).
 * 
 * Il est fait usage de la classe SimplesTableau pour acc\u00e9der aux m\u00e9thodes de cette classe.
 * @author Claire Th\u00e9bault
 */
public class TrisTableau {

    /**Variable globale, compteur du nombre d'op\u00e9rations
     */
    long cpt;

    /** Variable globale permettant l'acc\u00e8s aux m\u00e9thodes de la classe SimplesTableau
     */
    SimplesTableau monSpleTab = new SimplesTableau();

    
    /**
     * Le point d'entr\u00e9e du programme.
     */
    void principal() {

        // M\u00e9thodes de test
        testRechercheSeq();
        testVerifTri();
        testRechercheDicho();
        testTriSimple();
        testSeparer();
        testTriRapide();
        testCreerTabFreq();
        testTriParComptageFreq();
        testTriABulles();
        

        //M\u00e9thodes de tests d'efficacit\u00e9
        testRechercheSeqEfficacite();
        testRechercheDichoEfficacite();
        testTriSimpleEfficacite();
        testTriRapideEfficacite();
        testTriParComptageFreqEfficacite();
        testTriABullesEfficacite();
        
    }
        
        /**
        * Recherche s\u00e9quentielle d'une valeur dans un tableau. La valeur \u00e0 rechercher peut exister en plusieurs exemplaires mais la recherche
        * s'arr\u00eAte d\u00e8s qu'une premi\u00e8re valeur est trouv\u00e9e. On suppose que le tableau pass\u00e9 en param\u00e8tre est cr\u00e9\u00e9 et poss\u00e8de au moins une
        * valeur (nbElem &gt; 0). Ceci ne doit donc pas \u00eAtre v\u00e9rifi\u00e9.
        * @param leTab  le tableau dans lequel effectuer la recherche
        * @param nbElem  le nombre d'entiers que contient le tableau
        * @param aRech  l'entier \u00e0 rechercher dans le tableau
        * @return l'indice (&gt;=0) de la position de l'entier dans le tableau ou -1 s'il n'est pas pr\u00e9sent
        */
        int rechercheSeq(int[] leTab, int nbElem, int aRech) {

            int ret = -1;
            int i = 0;
            boolean trouve = false;
        
            while (i < nbElem && !trouve) {
            if (leTab[i] == aRech) {
                trouve = true;
                ret = i;
            }
            cpt++;
            i++;
            }
            return ret;
        }
        
        /**
        * test de la m\u00e9thode rechercheSeq
        */
        void testRechercheSeq() {
            System.out.println();
            System.out.println("*** testRechercheSeq *** ");

            //Cas Normaux
            System.out.println(" Cas normaux :  ");
            int[] tab1 = {2,4,6,0};
            testCasRerchercheSeq(tab1, 3, 4, 1);
            //Cas limites
            System.out.println(" Cas limites :  ");
            int[] tab6 = {233};
            testCasRerchercheSeq(tab6, 1, 4, -1);
            int[] tab7 = {233};
            testCasRerchercheSeq(tab7, 1, 233, 0);
            System.out.println();
        }

        /**
        * teste un appel de verifTab
        * @param leTab le tableau dont on doit v\u00e9rifier l'existence (leTab diff\u00e9rent de null)
        * @param nbElem le nombre d'\u00e9l\u00e9ments que contiendra le tableau, doit v\u00e9rifier la condition 0 &lt; nbElem &lt;= leTab.length
        * @param aRech  l'entier \u00e0 rechercher dans le tableau
        * @param repAtt resultat attendu
        **/ 
        void testCasRerchercheSeq (int[] leTab, int nbElem, int aRech, int repAtt) {
            int tmp;
            tmp = rechercheSeq(leTab, nbElem, aRech);

            if (tmp == repAtt) {   
                System.out.println("Test reussi");
            } else {
                System.out.println("Echec du test");            
            }
        }

        /**
        * Test de l'efficacite de la methode rechercheSeq
        **/
        void testRechercheSeqEfficacite() { // θ(n)
            System.out.println("*** testRechercheSeqEfficacite *** ");

            int n = 100000;
            int resultat;
            double tmp1, tmp2, tmpFinal;

            for(int i = 0; i < 4; i++) {
                int[] tab = new int[n];
                cpt = 0;
                tmp1 = System.nanoTime();
                rechercheSeq(tab, n, 38);
                tmp2 = System.nanoTime();
                tmpFinal = tmp2 - tmp1;
                tmpFinal = tmpFinal / 1000000;
                resultat = (int) (cpt/n);
                System.out.println(" n = " + n + " : " + tmpFinal + " ms, " 
                + resultat + " cpt/n");
                n = n * 2; 
            }
            System.out.println();
        }

        /**
        * V\u00e9rifie que le tableau pass\u00e9 en param\u00e8tre est tri\u00e9 par ordre croissant des valeurs. On suppose que le tableau pass\u00e9 en param\u00e8tre est
        * cr\u00e9\u00e9 et poss\u00e8de au moins une valeur (nbElem &gt; 0). Ceci ne doit donc pas \u00eAtre v\u00e9rifi\u00e9.
        * @param leTab le tableau \u00e0 v\u00e9rifier (tri\u00e9 en ordre croissant)
        * @param nbElem le nombre d'entiers pr\u00e9sents dans le tableau
        * @return true si le tableau est tri\u00e9
        */
        
        boolean verifTri(int[] leTab, int nbElem){
            boolean rep = true;
                for(int i = 0; i < nbElem - 1; i++) {
                    if (leTab[i] > leTab[i+1]) {
                        rep = false;
                    }
                }
            return rep;
        }

        /**
        * test de la m\u00e9thode verifTri
        */
        void testVerifTri () {
            System.out.println();
            System.out.println("*** testVerifTri *** ");

            //Cas Normaux
            System.out.println(" Cas normaux :  ");
            int[] tab1 = {2,4,6,0};
            testCasVerifTri(tab1, 4, false);
            int[] tab2 = {2,4,6,8};
            testCasVerifTri(tab2, 4, true);
            int[] tab3 = {-2,4,6,6};
            testCasVerifTri(tab3, 4, true);

            //Cas limites
            System.out.println(" Cas limites :  ");
            int[] tab4 = {2};
            testCasVerifTri(tab4, 1, true);
            int[] tab5 = {2,4};
            testCasVerifTri(tab5, 2, true);
            System.out.println();
        }

        /**
        * teste un appel de verifTri
        * @param leTab le tableau \u00e0 v\u00e9rifier (tri\u00e9 en ordre croissant)
        * @param nbElem le nombre d'entiers pr\u00e9sents dans le tableau
        * @param repAtt resultat attendu
        **/ 
        void testCasVerifTri (int[] leTab, int nbElem, boolean repAtt) {
            boolean tmp;
            tmp = verifTri(leTab, nbElem);

            if (tmp == repAtt) {   
                System.out.println("Test reussi");
            } else {
                System.out.println("Echec du test");            
            }
        }

        /**
        * Recherche dichotomique d'une valeur dans un tableau. On suppose que le tableau est tri\u00e9 par ordre croissant. La valeur \u00e0
        * rechercher peut exister en plusieurs exemplaires, dans ce cas, c'est la valeur \u00e0 l'indice le + faible qui sera trouv\u00e9. On suppose
        * \u00e9galement que le tableau pass\u00e9 en param\u00e8tre est cr\u00e9\u00e9 et poss\u00e8de au moins une valeur (nbElem &gt; 0). Ceci ne doit donc pas \u00eAtre
        * v\u00e9rifi\u00e9.
        * @param leTab le tableau tri\u00e9 par ordre croissant dans lequel effectuer la recherche)
        * @param nbElem le nombre d'entiers que contient le tableau
        * @param aRech l'entier \u00e0 rechercher dans le tableau
        * @return l'indice (&gt;=0) de la position de l'entier dans le tableau ou -1 s'il n'est pas pr\u00e9sent
        */
        int rechercheDicho(int[] leTab, int nbElem, int aRech) {
            // variables locales
            // indD = indice de d\u00e9but du sous-tableau
            // indF = indice de fin du sous-tableau
            // indM = indice milieu entre indD et indF
            int indD, indF, indM, ret;

            // initialisations
            indD = 0;
            indF = nbElem - 1;

            // boucle
            while ( indD != indF ) {
                indM = ( indD + indF ) / 2; // division enti\u00e8re !
                if ( aRech > leTab[indM] ) {
                    indD = indM + 1;
                } else {
                    indF = indM;
                }
                cpt++;
            }
            // terminaison : indD == indF forc\u00e9ment
            // MAIS leTab [indD] par forc\u00e9ment = aRech !!
            if ( aRech == leTab [indD] ) ret = indD;
            else ret = -1;
            return ret;
        }

        /**
        * Test de l'efficacite de la methode rechercheDicho
        */
        void testRechercheDichoEfficacite () { // θ(log2n)
            System.out.println("*** testRechercheDichoEfficacite *** ");

            int n = 100000;
            double resultat;
            double tmp1, tmp2, tmpFinal;

            for(int i = 0; i < 4; i++) {
                int[] tab = new int[n];
                cpt = 0;
                tmp1 = System.nanoTime();
                rechercheDicho(tab, n, 38);
                tmp2 = System.nanoTime();
                tmpFinal = tmp2 - tmp1;
                tmpFinal = tmpFinal / 1000000;
                resultat = (cpt/(Math.log(n)/Math.log(2)));
                System.out.println(" n = " + n + " : " + tmpFinal + " ms, " 
                + resultat + " cpt/log2n");
                n = n * 2; 
            }
            System.out.println();
        } 

        /**
        * test de la m\u00e9thode rechercheDicho
        */
        void testRechercheDicho () {
            System.out.println();
            System.out.println("*** testRechercheDicho *** ");

            //Cas Normaux
            System.out.println(" Cas normaux :  ");
            int[] tab1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            testCasRechercheDicho(tab1, 10, 5, 4);
            testCasRechercheDicho(tab1, 10, 1, 0);
            testCasRechercheDicho(tab1, 10, 10, 9);
            testCasRechercheDicho(tab1, 10, 11, -1);
            testCasRechercheDicho(tab1, 10, 0, -1);
            int[] tab3 = {1,1,1,1,1};
            testCasRechercheDicho(tab3, 5, 1, 0);

            //Cas limites
            System.out.println(" Cas limites :  ");
            int[] tab2 = {1};
            testCasRechercheDicho(tab2, 1, 1, 0);
            testCasRechercheDicho(tab2, 1, 2, -1);
            testCasRechercheDicho(tab2, 1, 0, -1);
            System.out.println();
        }

        /**
        * teste un appel de rechercheDicho
        * @param leTab le tableau tri\u00e9 par ordre croissant dans lequel effectuer la recherche)
        * @param nbElem le nombre d'entiers que contient le tableau
        * @param aRech l'entier \u00e0 rechercher dans le tableau
        * @param repAtt resultat attendu
        **/ 
        void testCasRechercheDicho(int[] leTab, int nbElem, int aRech, int repAtt) {
            int rep = rechercheDicho(leTab, nbElem, aRech);
            if (rep == repAtt) {
                System.out.println("Test reussi");
            } else {
                System.out.println("Echec du test");
            }   
        }

        /**
        * Tri par ordre croissant d'un tableau selon la m\u00e9thode simple : l'\u00e9l\u00e9ment minimum est plac\u00e9 en d\u00e9but de tableau (efficacit\u00e9 en n
        * carr\u00e9). On suppose que le tableau pass\u00e9 en param\u00e8tre est cr\u00e9\u00e9 et poss\u00e8de au moins une valeur (nbElem &gt; 0). Ceci ne doit donc pas
        * \u00eAtre v\u00e9rifi\u00e9.
        * @param leTab le tableau \u00e0 trier par ordre croissant
        * @param nbElem le nombre d'entiers que contient le tableau
        */
        void triSimple(int[] leTab, int nbElem) {
            int i = 0;
            int j = 0;
            int min = leTab[0];
            int indiceMin = 0;

            while (i < nbElem - 1) {
                j = i;
                indiceMin = i;
                min = leTab[i];
                while(j < nbElem){
                    if(min > leTab[j]){
                        min = leTab[j];
                        indiceMin = j;
                    }
                    cpt++;
                    j++; 
                }
                echange(leTab, i, indiceMin);
                i++;
            }
            cpt++;
        }

        /**
        * Test de l'efficacite de la methode triSimple
        **/
        void testTriSimpleEfficacite () { // θ(n2)
            System.out.println("*** testTriSimpleEfficacite *** ");

            int n = 1000;
            double resultat;
            double tmp1, tmp2, tmpFinal;

            for(int i = 0; i < 4; i++) {
                //creation d'un tableau tab qui contient que des int, le tableau fait la taille de n qui est lui m\u00eAme un int
                int[] tab = new int[n];

                //creation d'un tableau de taille n qui contient en ordre d\u00e9croissant des valeurs de 1 \u00e0 n
                for(int j = 0; j < n; j++){
                    tab[j] = n - j;
                }

                cpt = 0;
                tmp1 = System.nanoTime();
                triSimple(tab, n);
                tmp2 = System.nanoTime();
                tmpFinal = tmp2 - tmp1;
                tmpFinal = tmpFinal / 1000000;
                //calcul du resultat en faisant cpt divis\u00e9 par n au carr\u00e9
                resultat = cpt/(Math.pow(n, 2));
                System.out.println(" n = " + n + " : " + tmpFinal + " ms, " 
                + resultat + " cpt/n2");
                n = n * 2; 
            }
            System.out.println();
        }

        /**
        * test de la m\u00e9thode triSimple
        */
        void testTriSimple () {
            System.out.println();
            System.out.println("*** testTriSimple *** ");

            //Cas Normaux
            System.out.println(" Cas normaux :  ");
            int[] tab1 = {1, 2, 3, 4, 5};
            int[] tab2 = {1, 2, 3, 4, 5};
            testCasTriSimple(tab1, 5, tab2);
            int[] tab3 = {5, 4, 3, 2, 1};
            int[] tab4 = {1, 2, 3, 4, 5};
            testCasTriSimple(tab3, 5, tab4);
            int[] tab5 = {1, 1, 1, 1, 1};
            int[] tab6 = {1, 1, 1, 1, 1};
            testCasTriSimple(tab5, 5, tab6);
            int[] tab7 = {1,9,8,7,6,5,1,3,4,5};
            int[] tab8 = {1,1,3,4,5,5,6,7,8,9};
            testCasTriSimple(tab7, 10, tab8);
            int[] tab9 = {-5, -6,-4, 6,-4, 5, 4, 3, 2, 1};
            int[] tab10 = {-6,-5,-4,-4,1,2,3,4,5,6};
            testCasTriSimple(tab9, 10, tab10);

            //Cas limites
            System.out.println(" Cas limites :  ");
            int[] tab11 = {1};
            int[] tab12 = {1};
            testCasTriSimple(tab11, 1, tab12);
            int[] tab13 = {94,2};
            int[] tab14 = {2,94};
            testCasTriSimple(tab13, 2, tab14);
            System.out.println();
        }


        /**
        * teste un appel de triSimple
        * @param leTab le tableau \u00e0 trier par ordre croissant
        * @param nbElem le nombre d'entiers que contient le tableau
        * @param repAtt resultat attendu
        **/ 
        void testCasTriSimple (int[] leTab, int nbElem, int[] repAtt) {
            triSimple(leTab, nbElem);
            boolean egalite;
            egalite = Arrays.equals(leTab, repAtt);
            if (egalite) {   
                System.out.println("Test reussi");
            } else {
                System.out.println("Echec du test");            
            }
        }

        /**
        * Cette m\u00e9thode renvoie l'indice de s\u00e9paration du tableau en 2 parties par placement du pivot \u00e0 la bonne case.
        * @param tab le tableau des valeurs
        * @param indR indice Right de fin de tableau
        * @param indL indice Left de d\u00e9but de tableau
        * @return l'indice de s\u00e9paration du tableau
        */
        int separer(int[] tab, int indL, int indR) {
            boolean droiteGauche = true;

            while (indL != indR) {
                if (droiteGauche) {
                    if (tab[indR] < tab[indL]) {
                        echange(tab, indL, indR);
                        droiteGauche = false;
                    } else {
                        indR--;
                    }
                } else {
                    if (tab[indL] > tab[indR]) {
                        echange(tab, indL, indR);
                        droiteGauche = true;
                    } else {
                        indL++;
                    }
                }
                cpt++;
            }
            return indL;
        }

        /**
        * test de la m\u00e9thode separer
        */
        void testSeparer() {
            System.out.println();
            System.out.println("*** testSeparer *** ");

            //Cas Normaux
            System.out.println(" Cas normaux :  ");
            int[] tab1 = {1, 2, 3, 4, 5};
            testCasSeparer(tab1, 0, 4, 0);
            int[] tab2 = {5, 4, 3, 2, 1};
            testCasSeparer(tab2, 0, 4, 4);
            int[] tab3 = {1, 1, 1, 1, 1};
            testCasSeparer(tab3, 0, 4, 0);
            int[] tab4 = {1,9,8,7,6,5,1,3,4,5};
            testCasSeparer(tab4, 0, 9, 0);
            int[] tab5 = {-5, -6,-4, 6,-4, 5, 4, 3, 2, 1};
            testCasSeparer(tab5, 0, 9, 1);

            //Cas limites
            System.out.println(" Cas limites :  ");
            int[] tab6 = {1};
            testCasSeparer(tab6, 0, 0, 0);
            int[] tab7 = {94,2};
            testCasSeparer(tab7, 0, 1, 1);
            System.out.println();    
        }

        /**
        * teste un appel de separer
        * @param tab le tableau des valeurs
        * @param indR indice Right de fin de tableau
        * @param indL indice Left de d\u00e9but de tableau
        * @param repAtt r\u00e9sultat attendu
        **/ 
        void testCasSeparer (int[] tab, int indL, int indR, int repAtt) {
            int indiceSeparation = separer(tab, indL, indR);
            if (indiceSeparation == repAtt) {   
                System.out.println("Test reussi");
            } else {
                System.out.println("Echec du test");            
            }
        }

        /**
        * M\u00e9thode de tri r\u00e9cursive selon le principe de s\u00e9paration. La m\u00e9thode s'appelle elle-m\u00eAme sur les tableaux gauche et droite par
        * rapport \u00e0 un pivot.
        * @param tab le tableau sur lequel est effectu\u00e9 la s\u00e9paration
        * @param indL l'indice gauche de d\u00e9but de tableau
        * @param indR l'indice droite de fin de tableau
        */
        void triRapideRec(int[] tab, int indL, int indR) {
            // variable locale
            int indS; // indS = indice de s\u00e9paration
            indS = separer (tab, indL, indR );

            if ( (indS-1) > indL ) {
                triRapideRec ( tab, indL, (indS-1) );
            }

            if ( (indS+1) < indR ) {
                triRapideRec ( tab, (indS+1), indR );
            }
        }

        /**
        * Tri par ordre croissant d'un tableau selon la m\u00e9thode du tri rapide (QuickSort). On suppose que le tableau pass\u00e9 en param\u00e8tre est
        * cr\u00e9\u00e9 et poss\u00e8de au moins une valeur (nbElem &gt; 0). Ceci ne doit donc pas \u00eAtre v\u00e9rifi\u00e9. Cette m\u00e9thode appelle triRapideRec(...) qui elle
        * effectue r\u00e9ellement le tri rapide selon la m\u00e9thode de s\u00e9paration r\u00e9cursive.
        * @param leTab le tableau \u00e0 trier par ordre croissant
        * @param nbElem le nombre d'entiers que contient le tableau
        */
        void triRapide(int[] leTab, int nbElem) {
            triRapideRec (leTab, 0, (nbElem-1));
        }

        /**
        * test de la m\u00e9thode triRapide
        */
        void testTriRapide() {
            System.out.println();
            System.out.println("*** testTriRapide *** ");

            //Cas Normaux
            System.out.println(" Cas normaux :  ");
            int[] tab1 = {1, 2, 3, 4, 5};
            int[] repAtt1 = {1, 2, 3, 4, 5};
            testCasTriRapide(tab1, 5, repAtt1);
            int[] tab2 = {10, 9, 8, 6};
            int[] repAtt2 = {6, 8, 9, 10};
            testCasTriRapide(tab2, 4, repAtt2);
            int[] tab3 = {1, 1, 1, 1, 1};
            int[] repAtt3 = {1, 1, 1, 1, 1};
            testCasTriRapide(tab3, 5, repAtt3);
            int[] tab4 = {7,9,8,7,6,5,1,3,4,5};
            int[] repAtt4 = {1, 3, 4, 5, 5, 6, 7, 7, 8, 9};
            testCasTriRapide(tab4, 10, repAtt4);
            int[] tab5 = {-5, -6,-4, 6,-4, 5, 4, 3, 2, 1};
            int[] repAtt5 = {-6, -5, -4, -4, 1, 2, 3, 4, 5, 6};
            testCasTriRapide(tab5, 10, repAtt5);

            //Cas limites
            System.out.println(" Cas limites :  ");
            int[] tab6 = {1};
            int[] repAtt6 = {1};
            testCasTriRapide(tab6, 1, repAtt6);
            int[] tab7 = {94,2};
            int[] repAtt7 = {2, 94};
            testCasTriRapide(tab7, 2, repAtt7);
            System.out.println();
        }

        /**
        * teste un appel de triRapide
        * @param leTab le tableau \u00e0 trier par ordre croissant
        * @param nbElem le nombre d'entiers que contient le tableau
        * @param repAtt resultat attendu
        **/ 
        void testCasTriRapide (int[] leTab, int nbElem, int[] repAtt) {
            triRapide(leTab, nbElem);
            boolean egalite;
            egalite = Arrays.equals(leTab, repAtt);
            if (egalite) {   
                System.out.println("Test reussi");
            } else {
                System.out.println("Echec du test");            
            }
        }

        /**
        * Test de l'efficacite de la methode triRapide
        **/
        void testTriRapideEfficacite () { // θ(nlog2n)
            System.out.println("*** testTriRapideEfficacite *** ");

            int n = 100000;
            double resultat;
            double tmp1, tmp2, tmpFinal;

            for(int i = 0; i < 4; i++) {
                //creation d'un tableau tab qui contient que des int, le tableau fait la taille de n qui est lui m\u00eAme un int
                int[] tab = new int[n];

                //On met que des valeurs al\u00e9atoires dans le tableau tab
                monSpleTab.remplirAleatoire(tab, n, 0, n);
                

                //System.out.println(Arrays.toString(tab));
                cpt = 0;
                tmp1 = System.nanoTime();
                triRapide(tab, n);
                tmp2 = System.nanoTime();
                tmpFinal = tmp2 - tmp1;
                tmpFinal = tmpFinal / 1000000;
                //calcul du resultat en faisant cpt divis\u00e9 par nlog2n
                resultat = cpt/(n*(Math.log(n)/Math.log(2)));
                System.out.println(" n = " + n + " : " + tmpFinal + " ms, " 
                + resultat + " cpt/nlog2n");
                n = n * 2; 
            }
            System.out.println();
        }

        /**
        * A partir d'un tableau initial pass\u00e9 en param\u00e8tre "leTab", cette m\u00e9thode renvoie un nouveau tableau "tabFreq" d'entiers o\u00f9 chaque
        * case contient la fr\u00e9quence d'apparition des valeurs dans le tableau initial. Pour simplifier, on suppose que le tableau initial ne contient
        * que des entiers compris entre 0 et max (&gt;0). D\u00e8s lors le tableau "tabFreq" se compose de (max+1) cases et chaque case "i"
        * (0&lt;=i&lt;=max) contient le nombre de fois que l'entier "i" apparait dans le tableau initial. On suppose que le tableau pass\u00e9 en
        * param\u00e8tre est cr\u00e9\u00e9 et poss\u00e8de au moins une valeur (nbElem &gt; 0). Ceci ne doit donc pas \u00eAtre v\u00e9rifi\u00e9. Par contre, on v\u00e9rifiera que le
        * max du tableau initial est &gt;= min et que le min est &gt;= 0. Dans le cas contraire, renvoyer un tableau "null".
        * @param leTab le tableau initial
        * @param nbElem le nombre d'entiers pr\u00e9sents dans le tableau
        * @return le tableau des fr\u00e9quences de taille (max+1) ou null si la m\u00e9thode ne s'applique pas
        */
        int[] creerTabFreq(int[] leTab, int nbElem) {
            int max = 0;
            int i = 0;
            boolean neg = false;
            int[] tabFreq;

            while(i < nbElem && !neg) {
                if(leTab[i] < 0) {
                    neg = true;
                } else {
                    if(leTab[i] > max) {
                        max = leTab[i];
                    }
                }
                i++;
                cpt++;
            }
            if(neg==false) {
                tabFreq = new int[max+1];
                for(int j = 0; j < nbElem; j++) {
                    tabFreq[leTab[j]]++;
                }
            } else {
                System.err.println("Erreur creerTabFreq : le tab invalide");
                tabFreq = null;
            }
            return tabFreq;
        }

        /**
        * test de la m\u00e9thode creerTabFreq
        */
        void testCreerTabFreq() {
            System.out.println();
            System.out.println("*** testCreerTabFreq *** ");

            //Cas Normaux
            System.out.println(" Cas normaux :  ");
            int[] tab1 = {1, 2, 3, 4, 5};
            int[] repAtt1 = {0, 1, 1, 1, 1, 1};
            testCasCreerTabFreq(tab1, 5, repAtt1);
            int[] tab2 = {10, 9, 8, 6};
            int[] repAtt2 = {0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1};
            testCasCreerTabFreq(tab2, 4, repAtt2);
            int[] tab3 = {1, 1, 1, 1, 1};
            int[] repAtt3 = {0, 5};
            testCasCreerTabFreq(tab3, 5, repAtt3);
            int[] tab4 = {7,9,8,7,6,5,1,3,4,5};
            int[] repAtt4 = {0, 1, 0, 1, 1, 2, 1, 2, 1, 1};
            testCasCreerTabFreq(tab4, 10, repAtt4);

            //Cas limites
            System.out.println(" Cas limites :  ");
            int[] tab8 = {1,2};
            int[] repAtt8 = {0, 1, 1};
            testCasCreerTabFreq(tab8, 1, repAtt8);
            int[] tab7 = {1};
            int[] repAtt7 = {0, 1};
            testCasCreerTabFreq(tab7, 2, repAtt7);

            //Cas d'erreur
            System.out.println(" Cas d'erreur :  ");
            int[] tab5 = {-5,-6,-4, 6,-4, 5, 4, 3, 2, 1};
            int[] tab6 = null;
            testCasCreerTabFreq(tab5, 10, tab6);
            System.out.println();
        }

        /**
        * teste un appel de creerTabFreq
        * @param leTab le tableau initial
        * @param nbElem le nombre d'entiers pr\u00e9sents dans le tableau
        * @param repAtt resultat attendu
        **/ 
        void testCasCreerTabFreq (int[] leTab, int nbElem, int[] repAtt) {
            int[] tabFreq = creerTabFreq(leTab, nbElem);
            boolean egalite;
            egalite = Arrays.equals(tabFreq, repAtt);
            if (egalite) {   
                System.out.println("Test reussi");
            } else {
                System.out.println("Echec du test");            
            }
        }


        /**
        * Tri par ordre croissant d'un tableau selon la m\u00e9thode du tri par comptage de fr\u00e9quences. On suppose que le tableau pass\u00e9 en
        * param\u00e8tre est cr\u00e9\u00e9 et poss\u00e8de au moins une valeur (nbElem &gt; 0). Ceci ne doit donc pas \u00eAtre v\u00e9rifi\u00e9.
        * @param leTab le tableau \u00e0 trier par ordre croissant
        * @param nbElem le nombre d'entiers que contient le tableau
        */
        void triParComptageFreq(int[] leTab, int nbElem){
            int[] tabFreq = creerTabFreq(leTab, nbElem);
            
            if(tabFreq != null) {
                int i = 0;
                int repetition = 0;
                int p = 0;
                while(i < tabFreq.length) {
                    if(tabFreq[i] != 0) {
                        while(repetition<tabFreq[i]) {
                            leTab[p] = i;
                            p++;
                            repetition++;
                            cpt++;
                        }
                        repetition = 0;
                    }
                    i++;
                }
            } else {
                leTab = null;
                System.err.println("Erreur triParComptageFreq : le tab invalide");
                System.out.println(Arrays.toString(leTab));
            }
        }
    
        /**
        * test de la m\u00e9thode triParComptageFreq
        */
        void testTriParComptageFreq() {
            System.out.println();
            System.out.println("*** testTriParComptageFreq *** ");

            //Cas Normaux
            System.out.println(" Cas normaux :  ");
            int[] tab1 = {1, 2, 3, 4, 5};
            int[] repAtt1 = {1, 2, 3, 4, 5};
            testCasTriParComptageFreq(tab1, 5, repAtt1);
            int[] tab2 = {10, 9, 8, 6};
            int[] repAtt2 = {6, 8, 9, 10};
            testCasTriParComptageFreq(tab2, 4, repAtt2);
            int[] tab3 = {1, 1, 1, 1, 1};
            int[] repAtt3 = {1, 1, 1, 1, 1};
            testCasTriParComptageFreq(tab3, 5, repAtt3);
            int[] tab4 = {7,9,8,7,6,5,1,3,4,5};
            int[] repAtt4 = {1, 3, 4, 5, 5, 6, 7, 7, 8, 9};
            testCasTriParComptageFreq(tab4, 10, repAtt4);

            //Cas limites
            System.out.println(" Cas limites :  ");
            int[] tab7 = {1};
            int[] repAtt7 = {1};
            testCasTriParComptageFreq(tab7, 1, repAtt7);
            int[] tab8 = {1,2};
            int[] repAtt8 = {1, 2};
            testCasTriParComptageFreq(tab8, 2, repAtt8);
            System.out.println();
        }

        /**
        * teste un appel de triParComptageFreq
        * @param leTab le tableau \u00e0 trier par ordre croissant
        * @param nbElem le nombre d'entiers que contient le tableau
        * @param repAtt resultat attendu
        **/ 
        void testCasTriParComptageFreq (int[] leTab, int nbElem, int[] repAtt) {
            triParComptageFreq(leTab, nbElem);
            boolean egalite;
            egalite = Arrays.equals(leTab, repAtt);
            if (egalite) {   
                System.out.println("Test reussi");
            } else {
                System.out.println("Echec du test");            
            }
        }

        /**
        * Test de l'efficacite de la methode triParComptageFreq
        **/
        void testTriParComptageFreqEfficacite () { // θ(nlog2n)
            System.out.println("*** testTriParComptageFreqEfficacite *** ");

            int n = 100000;
            double resultat;
            double tmp1, tmp2, tmpFinal;

            for(int i = 0; i < 4; i++) {
                //creation d'un tableau tab qui contient que des int, le tableau fait la taille de n qui est lui m\u00eAme un int
                int[] tab = new int[n];

                //On met que des valeurs al\u00e9atoires mais inf\u00e9rieur \u00e0 n dans le tableau tab
                for(int j = 0; j < n; j++) {
                    tab[j] = (int)(Math.random()*n);
                }
                // On r\u00e9cup\u00e8re la valeur la plus grande du tableau tab
                int max = monSpleTab.leMax(tab, tab.length);
                
                //System.out.println(Arrays.toString(tab));
                cpt = 0;
                tmp1 = System.nanoTime();
                triParComptageFreq(tab, n); 
                tmp2 = System.nanoTime();
                tmpFinal = tmp2 - tmp1;
                tmpFinal = tmpFinal / 1000000;
                resultat = (double) cpt/(n+max);
                System.out.println(" n = " + n + " : " + tmpFinal + " ms, " 
                + resultat + " cpt/(n+max)");
                n = n * 2; 
            }
            System.out.println();
        }


        /**
        * Tri par ordre croissant d'un tableau selon la m\u00e9thode du tri \u00e0 bulles : tant que le tableau n'est pas tri\u00e9, permuter le contenu de 2
        * cases successives si leTab[i] &gt; leTab[i+1]. On suppose que le tableau pass\u00e9 en param\u00e8tre est cr\u00e9\u00e9 et poss\u00e8de au moins une valeur
        * (nbElem &gt; 0). Ceci ne doit donc pas \u00eAtre v\u00e9rifi\u00e9.
        * @param leTab le tableau \u00e0 trier par ordre croissant
        * @param nbElem le nombre d'entiers que contient le tableau
        */
        void triABulles(int[] leTab, int nbElem){
            boolean tri = false;
            int i = 0;
            int temp = 0;
            while(!tri) {
                tri = true;
                i = 0;
                while(i < nbElem-1) {
                    if(leTab[i] > leTab[i+1]) {
                        temp = leTab[i];
                        leTab[i] = leTab[i+1];
                        leTab[i+1] = temp;
                        tri = false;
                        cpt++;
                    }
                    i++;
                }
            }
        }

        /**
        * test de la m\u00e9thode triABulles
        */
        void testTriABulles() {
            System.out.println();
            System.out.println("*** testTriABulles *** ");

            //Cas Normaux
            System.out.println(" Cas normaux :  ");
            int[] tab1 = {1, 2, 3, 4, 5};
            int[] repAtt1 = {1, 2, 3, 4, 5};
            testCasTriABulles(tab1, 5, repAtt1);
            int[] tab2 = {10, 9, 8, 6};
            int[] repAtt2 = {6, 8, 9, 10};
            testCasTriABulles(tab2, 4, repAtt2);
            int[] tab3 = {1, 1, 1, 1, 1};
            int[] repAtt3 = {1, 1, 1, 1, 1};
            testCasTriABulles(tab3, 5, repAtt3);
            int[] tab4 = {7,9,8,7,6,5,1,3,4,5};
            int[] repAtt4 = {1, 3, 4, 5, 5, 6, 7, 7, 8, 9};
            testCasTriABulles(tab4, 10, repAtt4);
            int[] tab5 = {-5, -6,-4, 6,-4, 5, 4, 3, 2, 1};
            int[] repAtt5 = {-6, -5, -4, -4, 1, 2, 3, 4, 5, 6};
            testCasTriABulles( tab5, 10, repAtt5);

            //Cas limites
            System.out.println(" Cas limites :  ");
            int[] tab7 = {1};
            int[] repAtt7 = {1};
            testCasTriABulles(tab7, 1, repAtt7);
            int[] tab8 = {1,2};
            int[] repAtt8 = {1, 2};
            testCasTriABulles(tab8, 2, repAtt8);
            System.out.println();
        }
        
        /**
        * teste un appel de triABulles
        * @param leTab le tableau \u00e0 trier par ordre croissant
        * @param nbElem le nombre d'entiers que contient le tableau
        * @param repAtt r\u00e9sultat attendu
        **/ 
        void testCasTriABulles(int[] leTab, int nbElem, int[] repAtt) {
            triABulles(leTab, nbElem);
            boolean egalite;
            egalite = Arrays.equals(leTab, repAtt);
            if (egalite) {   
                System.out.println("Test reussi");
            } else {
                System.out.println("Echec du test");            
            }
        }

        /**
        * Test de l'efficacite de la methode triABulles
        **/
        void testTriABullesEfficacite () { // θ(n2)
            System.out.println("*** testTriABullesEfficacite *** ");

            int n = 1000;
            double resultat;
            double tmp1, tmp2, tmpFinal;

            for(int i = 0; i < 4; i++) {
                //creation d'un tableau tab qui contient que des int, le tableau fait la taille de n qui est lui m\u00eAme un int
                int[] tab = new int[n];

                //creation d'un tableau de taille n qui contient en ordre d\u00e9croissant des valeurs de 1 \u00e0 n
                for(int j = 0; j < n; j++){
                    tab[j] = n - j;
                }

                cpt = 0;
                tmp1 = System.nanoTime();
                triABulles(tab, n);
                tmp2 = System.nanoTime();
                tmpFinal = tmp2 - tmp1;
                tmpFinal = tmpFinal / 1000000;
                //calcul du resultat en faisant cpt divis\u00e9 par n au carr\u00e9
                resultat = cpt/(Math.pow(n, 2));
                System.out.println(" n = " + n + " : " + tmpFinal + " ms, " 
                + resultat + " cpt/n2");
                n = n * 2; 
            }
            System.out.println();
        }

        /**
         * Echange les contenus des cases du tableau pass\u00e9 en parametre, cases identifi\u00e9es par les indices ind1 et ind2
         * @param leTab le tableau
         * @param ind1 num\u00e9ro de la premiere case a \u00e9changer
         * @param ind2 num\u00e9ro de la deuxieme case a \u00e9changer
         */
        void echange(int[] leTab,int ind1, int ind2) {
            monSpleTab.echange(leTab, leTab.length, ind1, ind2);
        }
    }